package BL;

public class myPair {
	public int x;
	public int y;
	
	public myPair(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(myPair p){
		return (p.x==x & p.y==y);
	}
	
	public String toString(){
		return "<" + x + "," + y + ">";
	}

}
