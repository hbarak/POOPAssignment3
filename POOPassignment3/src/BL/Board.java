package BL;

import java.util.LinkedList;
import java.util.List;

public class Board {
	int size;
	int [][] board;
	List<Block> blocks;
	Block redBlock;
	final myPair winner = new myPair(3,6);//???
	

	//constructor.
	public Board(int size) {
		this.size = size;
		blocks = new LinkedList<Block>();
		board = createBoard(size + 2);
		redBlock = newBlock(3,1,2,true,2);
	}

	/**
	 * Creates a new Board, all the "inner" board is resets with 0's
	 * and all the the frame is resets with -2's
	 * @param size Size of the board.
	 * @return	A new clean Board.
	 */
	private int[][] createBoard(int size){
		int [][] newBoard = new int [size][size];
		for(int i=0; i<newBoard.length; i++){
			for(int j=0; j< newBoard[0].length; j++){
				if(i==0|i==(size-1)|j==0|j==(size-1)) newBoard[i][j] = -2;
				else newBoard[i][j] = 0;
			}
		}
		return newBoard;
	}
	
	/**
	 * Paints the board, assuming there was a verification for the move.
	 * @param block	The block to be painted.
	 * @param type	0 for empty, 1 for regular Block, and 2 for red Block.
	 */
	private void drawBlockOnBoard(Block block, int type){
		for (int i=0; i<block.getSize(); i++)
			board[block.getLocation()[i].x][block.getLocation()[i].y] = type;	
	}	
	
	private void deleteBlockFromBoard(Block block){
		drawBlockOnBoard(block, 0);
	}
	
	/**
	 * Checks validate of a move.
	 * Assumes(!!!) that the move is logically possible.
	 * @param block	The block to be moved.
	 * @param numOfMoves	Numbers of wanted moves.
	 * @param onDirection	true for right or down, for horizontal and vertical, false for opposite.
	 * @return
	 */
	private boolean isValidMove(Block block, int numOfMoves, boolean onDirection){
		myPair[] locations = block.getLocation();
		int temp;
		for(int i=0; i<numOfMoves; i++){
			if(block.getDirection()){	//for horizontal block
				if(onDirection){		//to the right
					temp = board[locations[locations.length].x][locations[locations.length].y + i];
				}
				else{					//left
					temp = board[locations[locations.length].x][locations[locations.length].y - i];
				}
			}
			else{						//for vertical block
				if(onDirection){		//down
					temp = board[locations[locations.length].x + i][locations[locations.length].y];
				}
				else{					//up
					temp = board[locations[locations.length].x - i][locations[locations.length].y];
				}
			}
			if(temp != 0) return false;		//anything but an empty cell.
		}
		return true;
	}
	
	
	/**
	 * Creates the new Block, adds it to the "blocks", and paints it on the board.
	 * @param i staring coordinates.
	 * @param j staring coordinates.
	 * @param size size of the Block.
	 * @param direction right is true, down is false
	 * @param type	1 for regular Block and 2 for red Block.
	 * @return	The new Block.
	 */
	private Block newBlock(int i, int j, int size, boolean direction, int type){
		Block temp = new Block(i, j, size, direction, type);
		//isValidMove?
		drawBlockOnBoard(temp, type);
		
	}
	

	
	public void removeBlock(Block block){
		/
	}
	
	public void move(Block block, int numOfSteps, boolean onDirection){
		if(isValidMove(block, numOfSteps, onDirection )){
			deleteBlockFromBoard(block);
			for(int i=0; i<block.getSize(); i++){
				if(block.getDirection()){	//for horizontal block
					if(onDirection){		//to the right
						block.getLocation()[i].y = block.getLocation()[i].y + numOfSteps;
					}
					else{					//left
						block.getLocation()[i].y = block.getLocation()[i].y - numOfSteps;
					}
				}
				else{						//for vertical block
					if(onDirection){		//down
						block.getLocation()[i].x = block.getLocation()[i].x + numOfSteps;
					}
					else{					//up
						block.getLocation()[i].x = block.getLocation()[i].x - numOfSteps;
					}
				}
			}
			drawBlockOnBoard(block, block.getType());
		}
	}
	
	private boolean updateData(){
		
	}
	
	
}
