package BL;

public class Block {
	private myPair[] location;
	private int i;
	private int j;
	private int size;
	private boolean direction;
	private int type;	//??


	/**
	 * 
	 * @param i staring coordinates.
	 * @param j staring coordinates.
	 * @param size size of the Block.
	 * @param direction right is true, down is false
	 */
	public Block(int i, int j, int size, boolean direction, int type) {
		this.size =  size;
		this.direction = direction;
		this.type = type;
		location = new myPair[size];
		for(int k=0; k<location.length; k++){
			location[k] = new myPair(i,j);
			if(direction) j++;
			else i++;
			
		}
	}
	
	
	//add get
	public int getSize(){
		return size;
	}
	
	public myPair[] getLocation(){
		return location;
	}
	
	public int getType(){
		return type;
	}
	
	public boolean getDirection(){
		return direction;
	}

}
